How to Run the code

1. Install Wamp/Xampp
2. Put the folder inside www. directory (For Wampp) or htdoc (For Xampp)
3. Run Wampp or Xampp Services
4. Go to localhost
5. Access the file from Your Projects section
6. From the website, select a date and time to find ISS
7. Press submit and please wait for a while as the processing will take a bit of time
8. After the processing is successful, it will show a page that will show all the result based on the user selection.
9. The result contain the time of the ISS, a link for the location of ISS, visibility, and country code

Extension implemented: None