<!DOCTYPE html>
<html>
  <head>
    <title>Find ISS</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="css/form.css" rel="stylesheet" />
    <script src="jquery-2.1.4.js"></script>

  </head>
  <body>

  
    <div class="testbox">
      
      <form id="issform" action="processdatetime.php" method="POST" onsubmit= "issbutton.classList.toggle('button--loading'); issbutton.disabled=true">
        <div class="banner">
            <h1>Where is ISS?</h1>
          </div>
        
        <p>Find ISS using date and time. <br>
        Result will show ISS location based on the input for every ten minutes before and after that for an hour.</p>
        

        <div class='field'>
          <i class="fas fa-calendar-alt"></i>
          <label for="issdate">Date</label>
          <input id="issdate" type="date" name="issdate" required/>
          
        </div>

        <div class='field'>
        <i class="fas fa-clock"></i>
          <label for="isstime">Time</label>
          <input id="isstime" type="time" name="isstime" required/>
        </div>
        <br>

        <div class="btn-block">
          <button id="issbutton" name="issbutton" class="button" type="submit">  
            <span class="button__text">Submit</span>
          </button>
        </div>
      </form>
    </div>
    


  </body>
</html>